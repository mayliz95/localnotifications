//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Laboratorio FIS on 10/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController,UNUserNotificationCenterDelegate {

    
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var outputTextField: UILabel!
    
    var notifcationmessage = "El texto es: "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Para recuperar
        //1. instanciar userDefaults
        let defaults = UserDefaults.standard
        
        //2. Acceder al valor por medio del KEY
        
        outputTextField.text = defaults.object(forKey: "label") as? String
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted,error) in }
    }


    @IBAction func saveButtonPressed(_ sender: Any) {
        outputTextField.text = inputTextField.text
        //Para guardar
        //1. instanciar userDefaults
        let defaults =  UserDefaults.standard
        
        //2. Guardar b¡variables en defaults
        //Int, Bool, Float, Double, String, Object!
        
        defaults.set(inputTextField.text, forKey: "label")
        
        sendNotification()
    }
    
    func sendNotification() {
        notifcationmessage += outputTextField.text!
        
        //1. Authorizathion request (está en didl Load)
        //2. Crear contenido de la notificación
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Tittle"
        content.subtitle = "Notification Subtitle"
        content.body = notifcationmessage
        
        //2.1 crear acciones
        let repeatAction = UNNotificationAction(identifier: "repeat", title: "Repetir", options: [])
        let changeMessageAction = UNTextInputNotificationAction(identifier: "change", title: "Cambiar", options: [])
        
        //2.2 Agregar las acciones
        
        let category = UNNotificationCategory(identifier: "actionCat", actions: [repeatAction,changeMessageAction], intentIdentifiers: [], options: [])
        //2.3 agregar el category identifier
        
        content.categoryIdentifier = "actionCat"
        
        //2.4 Agregar el category a un UnnuserNotificationcenter
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        //3. Definir un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //4. Definir un identificador para la notifcation
        
        let identifier = "Notification"
        
        //5. Crear un request
        
        let NotificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        //6. AÑADIR EL REQUEST AL UNUserNotificationcenter
        UNUserNotificationCenter.current().add(NotificationRequest) { (error) in }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "repeat":
            self.sendNotification()
            break
        case "change":
            let txtReponse = response as! UNTextInputNotificationResponse
            notifcationmessage += txtReponse.userText
            self.sendNotification()
            completionHandler()
            break
        default:
            break
        }
        print(response.actionIdentifier)
        completionHandler()
    }
}

